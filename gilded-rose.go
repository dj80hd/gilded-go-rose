package main

import "strings"

//Item is to remain unchanged for this example
type Item struct {
	name    string
	sellIn  int // Sell by this many days from now
	quality int // Quality: higher the better
}

//UpdateQuality peforms daily update to quality and sellIn for each Item
//Warning: Not idempotent; must be called only once a day to be accurate.
func UpdateQuality(items []*Item) {
	for _, item := range items {
		// Sulfuras quality is always 80, sellIn never changes
		if item.name != "Sulfuras, Hand of Ragnaros" {
			item.quality = item.qualityTomorrow()
			item.sellIn = item.sellIn - 1
		}
	}
}

//qualityTomorrow calculates Item quality one day from now
func (item *Item) qualityTomorrow() int {
	quality := item.quality

	if item.name == "Backstage passes to a TAFKAL80ETC concert" {
		if item.sellIn <= 0 {
			quality = 0
		} else if item.sellIn < 6 {
			quality = quality + 3
		} else if item.sellIn < 11 {
			quality = quality + 2
		} else {
			quality = quality + 1
		}

	} else if item.name == "Aged Brie" {
		if item.sellIn <= 0 {
			quality = quality + 2
		} else {
			quality = quality + 1
		}

	} else if strings.HasPrefix(item.name, "Conjured") {
		if item.sellIn <= 0 {
			quality = quality - 4
		} else {
			quality = quality - 2
		}

	} else {
		if item.sellIn <= 0 {
			quality = quality - 2
		} else {
			quality = quality - 1
		}
	}
	return limitRange(quality, 0, 50)
}

//confines intenger to a range
func limitRange(i int, min int, max int) int {
	if i > max {
		return max
	} else if i < min {
		return min
	}
	return i
}
