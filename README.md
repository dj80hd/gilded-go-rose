# Guilded-Rose Refactor in Go

A refactor of [go gilded-rose](https://github.com/emilybache/GildedRose-Refactoring-Kata/tree/master/go)

Approach:

* Use golang which favors clarity over cleverness.
* Modify texttest_fixture.go to _generate_ a `gilded-rose_test.go` file.
* Use this to refactor `UpdateQuality`.
* Add "Conjured" item code and tests assuming that only "Conjured" items have a name that begins with "Conjured" and that Conjured items degrade twice as fast as normal items.

## Testing

Original text fixure:

```shell
go run texttest_fixture.go gilded-rose.go
```

Unit tests in golang:

```shell
go test
```

100% coverage:

```shell
go test -coverprofile=coverage.out

go tool cover -html=coverage.out
```
